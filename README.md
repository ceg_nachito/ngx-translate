# NgxTranslate

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.1.

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Module

[Website](http://www.ngx-translate.com/)
[extractor](https://github.com/biesbjerg/ngx-translate-extract)
[github](https://github.com/ngx-translate/core)

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
