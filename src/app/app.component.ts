import { Component, OnInit } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { TranslationLoaderService } from "../app/services/translation-loader.service";

import { locale as English } from "./i18n/en";
import { locale as Spanish } from "./i18n/es";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit {
  title = "ngx-translate";
  param = { value: "John Doe" };

  constructor(
    public _translate: TranslateService,
    private _translationLoader: TranslationLoaderService
  ) {
    // this language will be used as a fallback when a translation isn't found in the current language
    this._translate.setDefaultLang("en");

    // Add languages
    this._translate.addLangs(["en", "es"]);

    // Set the navigation translations
    this._translationLoader.loadTranslations(English, Spanish);

    // the lang to use, if the lang isn't available, it will use the current loader to get them
    this._translate.use("en");
  }

  ngOnInit(): void {
    const browserLang = this._translate.getBrowserLang();
    // console.log('lang: ' + browserLang);
  }
}
